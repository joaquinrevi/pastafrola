package desafiosgrupales;
import java.lang.Math;
public class desafio13_prog01 {
    public static void main(String[] args) {
        int array[];
        array = new int[100];//Declaración del array
        
        for (int i = 0; i < 100; i++) {
            array[i] =(int) Math.floor(Math.random()*101);
        }//Se carga el arreglo con el número máximo + 1 para que el 100 también se incluya
        
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                if(array[i] == array[j] && i != j){
                    int rnd = (int) Math.floor(Math.random()*101);
                    array[i] = rnd;
                    i = 0;
                }
            }
        }//Se realiza la comprobación recorriendo el arreglo y comparando
        
       for(int num: array){
           System.out.println(num);
       }//Se muestra el resultado con un for each
    }
}
